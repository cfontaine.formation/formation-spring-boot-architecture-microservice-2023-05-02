package fr.dawan.springbootjwt.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springbootjwt.entities.Modele;

public interface ModeleRepository extends JpaRepository<Modele, Long> {

    List<Modele> findByMarqueNomLike(String nomMarque);

}
