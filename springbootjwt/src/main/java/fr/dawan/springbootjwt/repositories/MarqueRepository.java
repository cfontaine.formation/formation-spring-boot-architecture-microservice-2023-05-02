package fr.dawan.springbootjwt.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springbootjwt.entities.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {

    List<Marque> findByNomLike(String model);

}
