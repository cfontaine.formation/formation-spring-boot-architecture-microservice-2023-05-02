package fr.dawan.springbootjwt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springbootjwt.entities.Voiture;

public interface VoitureRepository extends JpaRepository<Voiture, Long> {

}
