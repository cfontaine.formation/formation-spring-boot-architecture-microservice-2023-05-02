package fr.dawan.springbootjwt.entities;

public enum Motorisation {
    DIESEL, ESSENCE, ELECTRIQUE, HYBRIDE
}
