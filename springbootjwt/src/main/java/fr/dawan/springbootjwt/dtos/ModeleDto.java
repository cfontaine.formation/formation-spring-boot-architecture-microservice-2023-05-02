package fr.dawan.springbootjwt.dtos;

import fr.dawan.springbootjwt.entities.Motorisation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ModeleDto {

    private long id;

    private String nom;

    private int puissance;

    private int nombrePlace;

    private Motorisation motorisation;

    private byte[] photo;

    private MarqueDto marque;
}
