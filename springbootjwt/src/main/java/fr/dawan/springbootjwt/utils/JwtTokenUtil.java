package fr.dawan.springbootjwt.utils;

import java.io.Serializable;
import java.util.Date;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Component
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private int expiration;
    
    // Génére le token JWT
    public String generateJwtToken(UserDetails userDetails) {
        return Jwts.builder().setSubject(userDetails.getUsername()).setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + expiration))
                .signWith(getSecretKey(), SignatureAlgorithm.HS256).compact();
    }

    // Test la validité du token ( username extrait de la charge utile du token, correspond au username du userDetails et le token n'est pas expiré)
    public boolean validateJwtToken(String token, UserDetails userDetails) {
        return getUsernameFromJwtToken(token).equals(userDetails.getUsername()) && !isTokenExpired(token);
    }

    // Extrait le userName de la charge utile du token (claim sub)
    public String getUsernameFromJwtToken(String token) {
        return extractAllClaims(token).getSubject();
    }

    // Permet de tester, si le token est expiré (claim exp)
    public boolean isTokenExpired(String token) {
        return extractAllClaims(token).getExpiration().before(new Date());
    }

    // Extrait tous les claims (claim -> paire clé/value) du la charge utile (payload) du token
    private Claims extractAllClaims(String token) {
        return Jwts.parserBuilder().setSigningKey(getSecretKey()).build().parseClaimsJws(token).getBody();
    }

    // Crée une instance de SecretKey à partir de la secret
    private SecretKey getSecretKey() {
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode(secret));
    }
}
