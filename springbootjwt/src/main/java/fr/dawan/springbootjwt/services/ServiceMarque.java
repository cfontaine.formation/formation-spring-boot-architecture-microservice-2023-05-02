package fr.dawan.springbootjwt.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springbootjwt.dtos.MarqueDto;

public interface ServiceMarque {

    List<MarqueDto> getAllMarque(Pageable page);

    MarqueDto getMarqueById(long id);

    List<MarqueDto> getMarqueByName(String nom);

    boolean deleteMarque(long id);

    MarqueDto saveOrUpdate(MarqueDto marqueDto);

}
