package fr.dawan.springbootjwt.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springbootjwt.dtos.MarqueDto;
import fr.dawan.springbootjwt.entities.Marque;
import fr.dawan.springbootjwt.repositories.MarqueRepository;
import fr.dawan.springbootjwt.services.ServiceMarque;

@Service
@Transactional
public class ServiceMarqueImpl implements ServiceMarque {

    @Autowired
    private MarqueRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<MarqueDto> getAllMarque(Pageable page) {
        return repository.findAll(page).stream().map(m -> mapper.map(m, MarqueDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public MarqueDto getMarqueById(long id) {
        return mapper.map(repository.findById(id).get(), MarqueDto.class);
    }

    @Override
    public List<MarqueDto> getMarqueByName(String nom) {
        return repository.findByNomLike(nom).stream().map(m -> mapper.map(m, MarqueDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public boolean deleteMarque(long id) {
        boolean isExist = repository.existsById(id);
        if (isExist) {
            repository.deleteById(id);
        }
        return isExist;
    }

    @Override
    public MarqueDto saveOrUpdate(MarqueDto marqueDto) {
        return mapper.map(repository.saveAndFlush(mapper.map(marqueDto, Marque.class)), MarqueDto.class);
    }

}
