package fr.dawan.springbootjwt.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springbootjwt.dtos.ModeleDto;
import fr.dawan.springbootjwt.entities.Modele;
import fr.dawan.springbootjwt.repositories.ModeleRepository;
import fr.dawan.springbootjwt.services.ServiceModele;

@Service
@Transactional
public class ServiceModeleImpl implements ServiceModele {

    @Autowired
    private ModeleRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<ModeleDto> getAllModele(Pageable page) {
        return repository.findAll(page).getContent().stream().map(m -> mapper.map(m, ModeleDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public ModeleDto getById(long id) {
        return mapper.map(repository.findById(id), ModeleDto.class);
    }

    @Override
    public boolean deleteModele(long id) {
        if(repository.existsById(id)) {
            repository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public ModeleDto saveOrUpdate(ModeleDto modeleDto) {
        Modele m = repository.saveAndFlush(mapper.map(modeleDto, Modele.class));
        return mapper.map(m, ModeleDto.class);
    }

}
