package fr.dawan.springbootjwt.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springbootjwt.dtos.ModeleDto;

public interface ServiceModele {

    List<ModeleDto> getAllModele(Pageable page);

    ModeleDto getById(long id);

    boolean deleteModele(long id);

    ModeleDto saveOrUpdate(ModeleDto modeleDto);

}
