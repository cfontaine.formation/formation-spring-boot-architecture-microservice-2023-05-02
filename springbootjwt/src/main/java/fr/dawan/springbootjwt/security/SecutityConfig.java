package fr.dawan.springbootjwt.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class SecutityConfig {
	
    @Autowired
    private JwtRequestFilter jwtFilter;

    // On crée un spring bean passwordEncoder pour Bcrypt
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

    @Bean
    AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration)
            throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }
	
	@Bean
	SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf(csrf -> csrf.disable()).cors(withDefaults())          // on désactive le crsf et le on active le cors
                .authorizeHttpRequests(auth -> auth.requestMatchers(    
                        HttpMethod.POST, "/authenticate").permitAll()   // on autorise sans authenfication les requête POST vers /authenticate
                        .requestMatchers(HttpMethod.GET).permitAll()    // on autorise sans authenfication toutes les requête GET
                        .anyRequest().authenticated())                  // toutes les autres urls nécéssite une authentification
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);    // Ajouter le filtre jwtFilter avant le  filtre  UsernamePasswordAuthenticationFilter	
		return http.build();
	}
}
