package fr.dawan.springbootjwt.security;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import fr.dawan.springbootjwt.utils.JwtTokenUtil;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String jwtToken = null;
        // On recupère le Bearer token dans l'entête Authorization de la requête
        String requestTokenHeader = request.getHeader("Authorization");
        // On test s'il existe et s'il commence par Bearer 
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            // On supprime Bearer, pour récupérer le token jwt 
            jwtToken = requestTokenHeader.substring(7);

            // On extrait le username de la charge utile du token JWT 
            String userName = jwtTokenUtil.getUsernameFromJwtToken(jwtToken);
            // On vérifie qu'il existe et que le usernamePasswordAuthenticationToken n'est pas présent dans le SecurityContextHolder
            if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
                // Si le token est valide, on configure Spring Security pour définir manuellement l'authentification
                if (jwtTokenUtil.validateJwtToken(jwtToken, userDetails)){
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                            userDetails, null, userDetails.getAuthorities());
                    usernamePasswordAuthenticationToken
                            .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    // Après avoir définie l'authentification dans le security context, on spécifie que l'utilisateur courant est authentifié.
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            }
        }
        filterChain.doFilter(request, response);
    }
}
