package fr.dawan.springbootjwt.security;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
//Correspond à la réponse retourner par l'authentification 
public class JwtResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String jwttoken;

}
