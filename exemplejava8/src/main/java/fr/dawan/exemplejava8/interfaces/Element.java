package fr.dawan.exemplejava8.interfaces;

public interface Element {

    default void freiner() {
        System.out.println("Element Méthode par défaut freiner");
    }
}
