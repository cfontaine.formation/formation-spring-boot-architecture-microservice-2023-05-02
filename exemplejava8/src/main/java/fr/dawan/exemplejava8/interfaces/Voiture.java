package fr.dawan.exemplejava8.interfaces;

public class Voiture implements Vehicule, Element {

    @Override
    public void accelerer() {
        System.out.println("La voiture accélère");
    }

    // Dans chaque interface Vehicule et Element
    // Il y a une méthode par défaut qui à la même signature
    // -> on est obligé de redéfinir la méthode 
    @Override
    public void freiner() {
        // Vehicule.super.freiner(); // choix 1 -> On utilise la méthode par défaut de Vehicule
        // Element.super.freiner();  // choix 2 -> On utilise la méthode par défaut d'élément
        // System.out.println("La voiture freine"); // choix 3 -> on écrit sa propre implémentation
    }

}
