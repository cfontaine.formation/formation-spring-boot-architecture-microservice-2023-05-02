package fr.dawan.exemplejava8.interfaces;

public interface Vehicule {
    
    // Variable d'interface
    int NB_PORTE=4; // implicitement public final static

    // Méthode static d'interface
    static int puissance() { // implicitement par défaut public
        return 8;
    }
    
    // Méthode d'interface par défaut
    default void freiner() {
        System.out.println("Méthode par défaut freiner de Vehicule");
    }
    
    void accelerer();
}
