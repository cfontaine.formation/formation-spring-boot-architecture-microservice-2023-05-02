package fr.dawan.exemplejava8.lambdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        
        // 1. une classe qui implemente l'interface
        Somme s = new Somme();
        int res = traitement(1, 2, s);
        System.out.println(res);
        
        // 2. classe Annonyme
        res = traitement(3, 4, new Operation() {

            @Override
            public int calcul(int a, int b) {
                return a + b;
            }
        });
        System.out.println(res);
        res = traitement(3, 4, new Operation() {

            @Override
            public int calcul(int a, int b) {
                return a - b;
            }
        });
        System.out.println(res);

        // 3. Avec Java 8 Expression lamba 8
        res = traitement(5, 2, (int v1, int v2) -> {
            return v1 + v2;
        });
        
        System.out.println(res);
        res = traitement(5, 2, (int v1, int v2) -> {
            return v1 * v2;
        });
        
        System.out.println(res);
        res = traitement(5, 2, (v1, v2) -> v1 * v2);
        System.out.println(res);

        // l'interface Runnable ne contient qu'une seule méthode, on peut utiliser
        // expression lambda pour créer un thread
        Thread t1 = new Thread(() -> System.out.println("Hello World!!"));
        t1.start();

        List<Integer> lst = Arrays.asList(5, 3, 1, -1, 10);

        // l'interface Comparator ne contient qu'une seule méthode, on peut utiliser
        // expression lambda pour trier la liste
        Collections.sort(lst, (a, b) -> a > b ? -1 : 1);
        for (int a : lst) {
            System.out.println(a);
        }

        // Référence de méthode static
        Message msg1 = () -> afficherMessage();
        msg1.afficher();

        // à la place de l'expression lambda, on peut une référence de méthode
        // pour une méthode static nom_de_la _classe::nom_methode
        Message msg2 = Main::afficherMessage;
        msg2.afficher();

        // Référence de méthode d'instance
        ShowMessage obj = new ShowMessage();
        Message msg3 = () -> obj.afficherMsg();
        msg3.afficher();

        Message msg4 = obj::afficherMsg;
        msg4.afficher();

        // Référence de méthode constructeur
        Supplier<ShowMessage> msg5 = () -> new ShowMessage();
        ShowMessage shm1 = msg5.get();
        System.out.println(shm1);

        Supplier<ShowMessage> msg6 = ShowMessage::new;
        ShowMessage shm2 = msg6.get();
        System.out.println(shm2);

        // Méthode forEach
        List<String> lstStr = Arrays.asList("John", "Jane", "Yves", "Michel", "Jimmy");

        for (String str : lstStr) {
            System.out.println(str);
        }

        lstStr.forEach((st) -> System.out.println(st));

        lstStr.forEach(System.out::println);

        // Stream
        // Méthode terminale collect
        List<Integer> lstNum = Arrays.asList(4, -1, 8, 5, 4, 3, 8, 1, 0, -4);
        List<Integer> lstr = lstNum.stream().map(i -> 2 * i).filter(x -> x > 0 & x <= 10).sorted()
                .collect(Collectors.toList());
        lstr.forEach(System.out::println);

        // Méthode terminale reduce
        int r = lstNum.stream().map(i -> 2 * i).filter(x -> x > 0 & x <= 10).sorted().reduce(0, (a, b) -> a + b);
        System.out.println(r);

        // Méthode terminale foreach
        List<Article> lstArt = new ArrayList<>();
        lstArt.add(new Article("Livre java", 40));
        lstArt.add(new Article("Téléphone", 600));
        lstArt.add(new Article("Livre git", 50));
        lstArt.add(new Article("stylo", 4));
        lstArt.add(new Article("Livre c#", 35));
        lstArt.add(new Article("Livre react", 40));

        lstArt.stream().filter(a -> a.getPrix() < 100.0 && a.getDescription().startsWith("Livre"))
                .sorted().forEach(System.out::println);


        // Optionnal
        // Création d'un optional
        Optional<String> optVide = Optional.empty();
        Optional<String> opt1 = Optional.of("Hello");

        // Tester le contenu
        System.out.println(optVide.isEmpty()); // true
        System.out.println(optVide.isPresent()); // false
        System.out.println(opt1.isPresent()); // true
        System.out.println(opt1.isEmpty()); // false

        // accéder au contenu -> get
        // si l'optionnal est vide -> exception lancer
        if (opt1.isPresent()) {
            System.out.println(opt1.get());
        }

        // Si l'Optional contient une valeur: Le consummer est exécuté
        opt1.ifPresent(stt -> System.out.println(stt));
        opt1.ifPresent(System.out::println);

        // Valeur par défaut si l'optional est vide
        System.out.println(optVide.orElse("la chaine est vide"));

        // Si l'Optional ne contient pas de valeur: Le supplier est exécuté
        System.out.println(optVide.orElseGet(() -> "c'est vide"));

        try {
            // Si l'Optional ne contient pas de valeur une exception est lancée
            optVide.orElseThrow(() -> new Exception("c'est vide"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int traitement(int a, int b, Operation op) {
        return op.calcul(a, b);
    }

    public static void afficherMessage() {
        System.out.println("Référence Méthode static");
    }
}
