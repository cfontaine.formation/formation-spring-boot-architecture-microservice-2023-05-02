package fr.dawan.exemplejava8.lambdas;

public class Somme implements Operation {

    @Override
    public int calcul(int a, int b) {
        return a+b;
    }

}
