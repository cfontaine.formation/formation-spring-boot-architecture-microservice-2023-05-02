package fr.dawan.exemplejava8.lambdas;

@FunctionalInterface
public interface Message {
    void afficher();
}
