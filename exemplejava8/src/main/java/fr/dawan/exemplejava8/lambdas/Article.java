package fr.dawan.exemplejava8.lambdas;

import java.io.Serializable;

public class Article implements Serializable,Comparable<Article> {

    private static final long serialVersionUID = 1L;

    private String description;

    private double prix;

    public Article() {
        super();
    }

    public Article(String description, double prix) {
        this.description = description;
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "Article [description=" + description + ", prix=" + prix + "]";
    }

    @Override
    public int compareTo(Article o) {
       if(prix>o.prix) {
           return 1;
       }else if(prix==o.prix) {
           return 0;
       }
           else {
               return -1;
           }

    }

}
