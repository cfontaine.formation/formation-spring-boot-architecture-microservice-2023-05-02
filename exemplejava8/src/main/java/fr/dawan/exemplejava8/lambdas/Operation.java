package fr.dawan.exemplejava8.lambdas;

@FunctionalInterface
public interface Operation {
    
    int calcul (int a, int b);
    
}
