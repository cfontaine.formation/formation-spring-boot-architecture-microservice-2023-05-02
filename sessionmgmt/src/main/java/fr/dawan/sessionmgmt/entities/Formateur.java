package fr.dawan.sessionmgmt.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;
@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="formateurs")
public class Formateur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(nullable=false,length=40)
    @NonNull
    private String prenom;
    
    @Column(nullable=false,length=40)
    @NonNull
    private String nom;
    
    @Column(nullable=false , unique = true)
    @NonNull
    private String email;
    
    @ManyToOne
    @Exclude
    private Centre centre;
    
    @ManyToMany
    @Exclude
    private List<Formation> formations=new ArrayList<>();
    
    @OneToMany(mappedBy = "formateur")
    @Exclude
    private List<Session> sessions=new ArrayList<>();
}
