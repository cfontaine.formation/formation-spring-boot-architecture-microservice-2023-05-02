package fr.dawan.sessionmgmt.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="centres")
public class Centre implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(length = 50, nullable=false, name="ville_implementation")
    private String villeImplementation;
    
    @Column(nullable=false)
    private String adresse;
    
    @Column(nullable = false,name="nombre_salle")
    private int nombreSalle;
    
    @Lob
    @Column(length=65536)
    @Exclude
    private byte[] photo;
    
    @OneToMany(mappedBy = "centre")
    @Exclude
    private List<Formateur> formateurs =new ArrayList<>();

    @OneToMany(mappedBy = "centre")
    @Exclude
    private List<Session> sessions=new ArrayList<>();
    
    public Centre(String villeImplementation, String adresse, int nombreSalle) {
        this.villeImplementation = villeImplementation;
        this.adresse = adresse;
        this.nombreSalle = nombreSalle;
    }
}
