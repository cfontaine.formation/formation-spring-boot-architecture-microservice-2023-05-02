package fr.dawan.sessionmgmt.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="formations")
public class Formation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(length=6,name="ref")
    private  String reference;
    
    @Version
    private int version;
    
    @Column(nullable=false)
    private int duree;
    
    @Column(nullable = false)
    private String intitule;
    
    @Column(nullable = false)
    private double prix;
    
    @ManyToMany(mappedBy = "formations")
    @Exclude
    private List<Formateur> formateurs=new ArrayList<>();
    
    @OneToMany(mappedBy = "formation")
    @Exclude
    private List<Session> sessions=new ArrayList<>();

        
    public Formation(String reference, int duree, String intitule, double prix) {
        this.reference = reference;
        this.duree = duree;
        this.intitule = intitule;
        this.prix = prix;
    }
    
}
