package fr.dawan.sessionmgmt.entities;

import java.io.Serializable;
import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="sessions")
public class Session implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(name="date_debut",nullable=false)
    @NonNull
    private LocalDate dateDebut;
    
    @Column(name="date_fin",nullable=false)
    @NonNull
    private LocalDate dateFin;
    
    @ManyToOne
    @Exclude
    private Centre centre;
    
    @ManyToOne
    @Exclude
    private Formation formation;
    
    @ManyToOne
    @Exclude
    private Formateur formateur;
}
