package fr.dawan.sessionmgmt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.sessionmgmt.entities.Formation;

public interface FormationRepository extends JpaRepository<Formation, String> {

}
