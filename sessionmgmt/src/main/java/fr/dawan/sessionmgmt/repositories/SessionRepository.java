package fr.dawan.sessionmgmt.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.sessionmgmt.entities.Session;

public interface SessionRepository extends JpaRepository<Session, Long> {
    
    @Query("Select s FROM Session s WHERE s.dateDebut<=:dateR AND s.dateFin>=:dateR")
    List<Session> findByDateSession(@Param("dateR") LocalDate dateR);
    
    List<Session> findByFormationReference(String reference);
    
    List<Session> findByFormationIntituleLike(String motif);
    
    List<Session> findByCentreVilleImplementation(String villeImp);

    List<Session> findByFormateurEmail(String email);
    
}
