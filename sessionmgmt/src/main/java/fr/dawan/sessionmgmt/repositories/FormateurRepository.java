package fr.dawan.sessionmgmt.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.sessionmgmt.entities.Formateur;

public interface FormateurRepository extends JpaRepository<Formateur, Long> {
    List<Formateur> findByCentreVilleImplementation(String villeImp);
    
    @Query("SELECT f FROM Formateur f Join f.formations fo WHERE fo.reference=:ref")
    List<Formateur> findByFormation(@Param("ref") String referenceFormation);
}
