package fr.dawan.sessionmgmt.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.sessionmgmt.entities.Centre;

public interface CentreRepository extends JpaRepository<Centre, Long> {
   
    List<Centre> findByNombreSalleGreaterThan(int nbSalle);

}
