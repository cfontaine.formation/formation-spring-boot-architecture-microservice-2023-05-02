package fr.dawan.sessionmgmt;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import fr.dawan.sessionmgmt.repositories.FormateurRepository;
import fr.dawan.sessionmgmt.repositories.SessionRepository;
@Component
public class RepositoryRunner implements CommandLineRunner {

    @Autowired
    private SessionRepository repo;
    
    @Autowired
    private FormateurRepository formateurRepo;
    
    @Override
    public void run(String... args) throws Exception {
      repo.findByDateSession(LocalDate.of(2023, 5, 3)).forEach(System.out::println);
      
      repo.findByFormationReference("NET987").forEach(System.out::println);
    
      repo.findByFormationIntituleLike("%Initiation").forEach(System.out::println);
    
      repo.findByFormateurEmail("jdoe@dawan.com").forEach(System.out::println);
    
      formateurRepo.findByCentreVilleImplementation("Paris").forEach(System.out::println);
      
      formateurRepo.findByFormation("NET987").forEach(System.out::println);
    }

}
