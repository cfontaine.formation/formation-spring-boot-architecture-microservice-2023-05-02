package fr.dawan.sessionmgmt.services.implement;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.sessionmgmt.dtos.CentreDto;
import fr.dawan.sessionmgmt.entities.Centre;
import fr.dawan.sessionmgmt.repositories.CentreRepository;
import fr.dawan.sessionmgmt.services.CentreService;

@Service
@Transactional
public class CentreServiceImpl implements CentreService {

    @Autowired
    private CentreRepository repository;
    
    @Autowired
    private ModelMapper mapper;
    
    @Override
    public List<CentreDto> getAllCentre(Pageable page) {
        return repository.findAll(page).getContent().stream().map(c -> mapper.map(c,CentreDto.class)).collect(Collectors.toList());
    }

    @Override
    public CentreDto getCentreById(long id) {
        return mapper.map(repository.findById(id).get(),CentreDto.class);
    }

    @Override
    public List<CentreDto> getCentreByNbSalle(int nombreSalle) {
        return repository.findByNombreSalleGreaterThan(nombreSalle).stream().map(c -> mapper.map(c,CentreDto.class)).collect(Collectors.toList());
    }

    @Override
    public CentreDto saveOrUpdate(CentreDto centre) {
        return mapper.map(repository.saveAndFlush(mapper.map(centre, Centre.class)),CentreDto.class);
    }

    @Override
    public void delete(long id) {
        repository.deleteById(id);
    }

}
