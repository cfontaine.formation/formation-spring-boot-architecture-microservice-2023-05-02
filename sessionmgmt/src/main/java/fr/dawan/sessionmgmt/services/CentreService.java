package fr.dawan.sessionmgmt.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.sessionmgmt.dtos.CentreDto;

public interface CentreService {
    List<CentreDto> getAllCentre(Pageable page);

    CentreDto getCentreById(long id);

    List<CentreDto> getCentreByNbSalle(int nombreSalle);

    CentreDto saveOrUpdate(CentreDto centre);

    void delete(long id);
}
