package fr.dawan.sessionmgmt.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class CentreDto{

    private long id;

    private int version;

    private String villeImplementation;

    private String adresse;

    private int nombreSalle;

    private byte[] photo;
}