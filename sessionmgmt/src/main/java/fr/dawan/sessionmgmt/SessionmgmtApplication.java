package fr.dawan.sessionmgmt;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SessionmgmtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SessionmgmtApplication.class, args);
	}

	@Bean
	ModelMapper moddelMapper() {
	    return new ModelMapper();
	}
}
