package fr.dawan.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.services.MarqueService;
//@Component
//@Order(2)
public class ServiceRunner implements CommandLineRunner {

    @Autowired
    MarqueService marqueService;
    
    @Override
    public void run(String... args) throws Exception {
      System.out.println("Service Runner");
      marqueService.getAllMarque().forEach(System.out::println);
      
      System.out.println(marqueService.getMarqueById(2L));
      
      MarqueDto md=new MarqueDto();
      md.setNom("Lancia");
      MarqueDto md2= marqueService.saveOrUpdate(md);
     
      md2.setNom("Renault");
      marqueService.saveOrUpdate(md2);
      
      marqueService.deleteMarque(md2.getId());
    }

}
