package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "modeles")
public class Modele implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(length = 50, nullable = false)
    private String nom;

    @Column(nullable = false)
    private int puissance;

    @Column(nullable = false, name = "nombre_place")
    private int nombrePlace;

    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private Motorisation motorisation;

    @Lob
    @Column(length = 65536)
    @Exclude
    private byte[] photo;

    @ManyToOne
    @Exclude
    private Marque marque;

    @OneToMany(mappedBy = "modele")
    @Exclude
    private List<Voiture> voitures = new ArrayList<>();

    public Modele(String nom, int puissance, int nombrePlace, Motorisation motorisation) {
        this.nom = nom;
        this.puissance = puissance;
        this.nombrePlace = nombrePlace;
        this.motorisation = motorisation;
    }

}
