package fr.dawan.springboot.entities;

public enum Motorisation {
    DIESEL, ESSENCE, ELECTRIQUE, HYBRIDE
}
