package fr.dawan.springboot.dtos;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MarqueDto {

    @XmlAttribute
    private long id;

    // @JsonProperty("name-marque")
    private String nom;
}
