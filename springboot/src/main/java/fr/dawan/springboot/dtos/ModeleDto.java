package fr.dawan.springboot.dtos;

import fr.dawan.springboot.entities.Motorisation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class ModeleDto {

    private long id;

    private String nom;

    private int puissance;

    private int nombrePlace;

    private Motorisation motorisation;

    private byte[] photo;

    private MarqueDto marque;
}
