package fr.dawan.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import fr.dawan.springboot.entities.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Long> {

    // Procédure Stockée
    //SQL
    @Query(nativeQuery = true, value="CALL GET_COUNT_BY_PRIX(:prixMin)")
    int countByPrix(@Param("prixMin")double prixMin);

    // Appel Explicite JPA
    @Procedure("GET_COUNT_BY_PRIX2")
    int countByPrix2(double prixMin);
    
    // Appel Implicite JPA
    @Procedure
    int get_count_by_prix2(double prixMin);
}
