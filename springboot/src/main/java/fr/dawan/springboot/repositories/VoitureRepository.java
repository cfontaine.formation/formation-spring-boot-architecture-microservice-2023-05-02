package fr.dawan.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springboot.entities.Voiture;

public interface VoitureRepository extends JpaRepository<Voiture, Long> {

}
