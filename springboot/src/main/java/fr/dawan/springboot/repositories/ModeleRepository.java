package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.springboot.entities.Modele;

public interface ModeleRepository extends JpaRepository<Modele, Long> {
   
    List<Modele> findByNombrePlaceEquals(int nbPlace);
    
    List<Modele> findByMarqueNom(String nomMarque);
    
    @Query(  "SELECT m FROM Modele m WHERE m.marque.nom=:nommarque")
    List<Modele> findNomMarque(@Param("nommarque")String nomMarque);
    
    @Query(nativeQuery = true,value="SELECT * FROM modeles WHERE nombre_place=:nbplace")
    List<Modele> getNombrePlaceSQL(@Param("nbplace")int nbPlace);
}
