package fr.dawan.springboot;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import fr.dawan.springboot.services.EmailService;

//@Component
//@Order(3)
public class EmailRunner implements CommandLineRunner {

    @Autowired
    EmailService service;

    @Override
    public void run(String... args) throws Exception {
        // Email texte
        service.sendSimpleMail("Le contenu du mail", "Un email de test", "john.doe@dawan.com", "noreply@dawan.com");
        
        // Email HTML
        String html = "<html><body><h1>exemple de message HTML</h1></body></html>";
        service.sendHTMLMail(html, "Un email HTML de test", "john.doe@dawan.com", "noreply@dawan.com");
        
        // Email HTML template
        String mailfrom="jdoe@dawan.com";
        Map<String,Object> modelMap=new HashMap<>();
        modelMap.put("prenom", "John");
        modelMap.put("email", mailfrom);
        service.sendTemplateMail("mailTemplate.ftlh", modelMap,"Un email HTML de test (template)", mailfrom, "noreply@dawan.com");
    
        // Email avec pièce jointe
        File logo=new File("C:\\Dawan\\Formations\\spring boot\\springboot\\Logo dawan.jpg");
        service.sendMailAttachement("Le contenu du mail", "Un email de test avec pièce-jointe", "john.doe@dawan.com", "noreply@dawan.com", logo);
    }

}
