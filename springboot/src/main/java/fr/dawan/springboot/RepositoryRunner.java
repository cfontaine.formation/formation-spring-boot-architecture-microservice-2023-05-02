package fr.dawan.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import fr.dawan.springboot.entities.Marque;
import fr.dawan.springboot.repositories.EmployeCustomRepository;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.repositories.ModeleRepository;
import fr.dawan.springboot.repositories.ProduitRepository;

//@Component
//@Order(1)
public class RepositoryRunner implements CommandLineRunner {
    @Autowired
    private ModeleRepository modeleRepo;

    @Autowired
    private MarqueRepository marqueRepo;

    @Autowired
    private ProduitRepository produitRepo;

    @Autowired
    private EmployeCustomRepository employeRepo;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Repository Runner");

        marqueRepo.findAll().forEach(System.out::println);
        
        modeleRepo.findAll().forEach(System.out::println);

        // Procédure stockée
        System.out.println(produitRepo.countByPrix(20.0));
        System.out.println(produitRepo.countByPrix2(20.0));
        System.out.println(produitRepo.get_count_by_prix2(20.0));

        // Custom repository
        employeRepo.findBy("John", null).forEach(System.out::println);
        employeRepo.findBy(null, "Doe").forEach(System.out::println);
        employeRepo.findBy("John", "Doe").forEach(System.out::println);

        // Audit d'entité
        Marque m=new Marque();
        m.setNom("Toyota");
        marqueRepo.saveAndFlush(m);
        
        Marque m2 = marqueRepo.findById(5L).get();
        m2.setNom("Nissan");
        marqueRepo.saveAndFlush(m2);
    }

}
