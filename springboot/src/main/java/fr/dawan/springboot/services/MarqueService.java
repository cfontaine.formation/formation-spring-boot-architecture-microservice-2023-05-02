package fr.dawan.springboot.services;

import java.util.List;

import fr.dawan.springboot.dtos.MarqueDto;

public interface MarqueService {

    List<MarqueDto> getAllMarque();

    MarqueDto getMarqueById(long id);

    List<MarqueDto> getMarqueByName(String nom);

    boolean deleteMarque(long id);

    MarqueDto saveOrUpdate(MarqueDto marqueDto);

}
