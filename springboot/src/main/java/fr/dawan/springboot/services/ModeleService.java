package fr.dawan.springboot.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dtos.ModeleDto;

public interface ModeleService {

    List<ModeleDto> getAllModele(Pageable page);

    ModeleDto getById(long id);

    boolean deleteModele(long id);

    ModeleDto saveOrUpdate(ModeleDto modeleDto);

}
