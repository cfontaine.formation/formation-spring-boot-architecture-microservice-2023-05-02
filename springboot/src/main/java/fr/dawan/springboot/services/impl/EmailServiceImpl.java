package fr.dawan.springboot.services.impl;

import java.io.File;
import java.io.StringWriter;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import fr.dawan.springboot.services.EmailService;
import freemarker.template.Configuration;
import jakarta.mail.Message;
import jakarta.mail.internet.InternetAddress;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    JavaMailSender sender;

    @Autowired
    private Configuration configuration; // utiliser par freemarker

    // Envoie d'email texte
    @Override
    public void sendSimpleMail(String content, String title, String to, String from) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setFrom(from);
        message.setSubject(title);
        message.setText(content);
        sender.send(message);
    }

    // Envoie d'email hmtl
    @Override
    public void sendHTMLMail(String contentHtml, String title, String to, String from) {
//        MimeMessagePreparator messagePreparator=new MimeMessagePreparator() {
//            
//            @Override
//            public void prepare(MimeMessage mimeMessage) throws Exception {
//                mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
//                mimeMessage.setFrom(from);
//                mimeMessage.setSubject(title,"utf-8");
//                mimeMessage.setText(contentHtml,"utf-8","html");
//            }
//        };
//        sender.send(messagePreparator);

        // Idem en utilisant une expression lambda
        sender.send(mimeMessage -> {
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            mimeMessage.setFrom(from);
            mimeMessage.setSubject(title, "utf-8");
            mimeMessage.setText(contentHtml, "utf-8", "html");
        });

    }

    // Envoie d'email html avec un moteur de template freemarker (on peut aussi
    // utiliser thymeleaf)
    @Override
    public void sendTemplateMail(String template, Map<String, Object> modelMap, String title, String to, String from)
            throws Exception {
        StringWriter messageHTML = new StringWriter();
        configuration.getTemplate(template).process(modelMap, messageHTML);

        sendHTMLMail(messageHTML.toString(), title, to, from);
    }

    // Envoie d'email avec une pièce jointe
    @Override
    public void sendMailAttachement(String content, String title, String to, String from, File attachement) {
        sender.send(mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            message.setTo(to);
            message.setFrom(from);
            message.setSubject(title);
            message.setText(content);
            message.addAttachment(attachement.getName(), attachement);
        });

    }
}
