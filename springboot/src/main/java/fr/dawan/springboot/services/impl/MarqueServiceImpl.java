package fr.dawan.springboot.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.entities.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.MarqueService;

@Service
@Transactional
public class MarqueServiceImpl implements MarqueService {

    @Autowired
    private MarqueRepository repository;

    @Autowired
    private ModelMapper mapper;

//    @Override
//    public List<MarqueDto> getAllMarque() {
//       List<Marque> lstMarque=repository.findAll();
//       List<MarqueDto> lstMarqueDto=new ArrayList<>();
//       for(Marque m : lstMarque) {
//           MarqueDto md=mapper.map(m,MarqueDto.class);
//           lstMarqueDto.add(md);
//        }
//        return lstMarqueDto;
//    }

    @Override
    public List<MarqueDto> getAllMarque() {
//       List<Marque> lstMarque=repository.findAll();
//       return lstMarque.stream().map(m -> mapper.map(m, MarqueDto.class)).collect(Collectors.toList());
        return repository.findAll().stream().map(m -> mapper.map(m, MarqueDto.class)).collect(Collectors.toList());
    }

    @Override
    public MarqueDto getMarqueById(long id) {
        Marque marque = repository.findById(id).get();
        return mapper.map(marque, MarqueDto.class);
    }

    @Override
    public List<MarqueDto> getMarqueByName(String nom) {
        return repository.findByNomLike(nom).stream().map(m -> mapper.map(m, MarqueDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public boolean deleteMarque(long id) {
        boolean isExist = repository.existsById(id);
        if (isExist) {
            repository.deleteById(id);
        }
        return isExist;
    }

    @Override
    public MarqueDto saveOrUpdate(MarqueDto marqueDto) {
        Marque m = mapper.map(marqueDto, Marque.class);
        Marque mr = repository.saveAndFlush(m);
        return mapper.map(mr, MarqueDto.class);
    }

}
