package fr.dawan.springboot.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
        // return NoOpPasswordEncoder.getInstance();
    }

    // Uniquement authentification
 //   @Bean
//    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
//        http.csrf(csrf -> csrf.disable())
//                .authorizeHttpRequests(auth -> auth.requestMatchers(HttpMethod.DELETE).authenticated()
//                        .requestMatchers(HttpMethod.POST).authenticated()
//                        .requestMatchers(HttpMethod.PUT).authenticated()
//                        .anyRequest().permitAll())
//                .httpBasic(withDefaults());
//        return http.build();
//    }
    
    // Authentification et Autorities
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf(csrf -> csrf.disable())
                .authorizeHttpRequests(auth -> auth.requestMatchers(HttpMethod.DELETE).hasAnyAuthority("DELETE","SUPER")
                        .requestMatchers(HttpMethod.POST).hasAnyAuthority("WRITE","SUPER")
                        .requestMatchers(HttpMethod.PUT).hasAnyAuthority("WRITE","SUPER")
                        .requestMatchers(HttpMethod.GET).hasAnyAuthority("READ","SUPER")
                        .anyRequest().permitAll())
                .httpBasic(withDefaults());
        return http.build();
    }
    
}