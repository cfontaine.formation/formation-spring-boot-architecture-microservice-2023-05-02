package fr.dawan.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.HelloConfig;

@RestController
public class HelloWorldController {

//    @Value("${hello.message}")
//    private String message;

    @Autowired
    private HelloConfig hc;

    @RequestMapping("/hello")
    public String hello() {
        return hc.getMessage() + " " + hc.getPrenom() + " " + hc.getNom();
    }

}
