package fr.dawan.springboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.services.MarqueService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/v1/marques")
@Tag(name = "Marques", description = "L'api des marques")
public class MarqueController {

    @Autowired
    MarqueService service;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> getAll() {
        return service.getAllMarque();
    }

    @Operation(summary = "Trouver les marques à partir de leurs id", description = "retourne une marque", tags = "Marques")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Opération reussit", content = @Content(schema = @Schema(implementation = MarqueDto.class))),
            @ApiResponse(responseCode = "404", description = "Marque non trouvée") })
    @GetMapping(value = "/{id:[0-9]+}", produces = { MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE })
    public ResponseEntity<MarqueDto> getById(
            @Parameter(description = "L'id de la marque", required = true, allowEmptyValue = false) @PathVariable long id) {
        try {
            return ResponseEntity.ok(service.getMarqueById(id)); // 200
        } catch (Exception e) {
            return ResponseEntity.notFound().build(); // 404
        }
    }

    @GetMapping(value = "/{marque:^[a-zA-Z][a-zA-Z0-9]+$}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> getByMarque(@PathVariable String marque) {
        return service.getMarqueByName("%" + marque + "%");
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteMarque(@PathVariable long id) {
        if (!service.deleteMarque(id)) {
            return new ResponseEntity<>("La marque Id=" + id + " n'existe pas", HttpStatus.NOT_FOUND);// 404
        } else {
            return new ResponseEntity<>("La marque Id=" + id + " est supprimée", HttpStatus.OK); // 200
            // return ResponseEntity.noContent().build(); // 204
        }

    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto addMarque(@RequestBody MarqueDto marqueDto) {
        return service.saveOrUpdate(marqueDto);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto updateMarque(@PathVariable long id, @RequestBody MarqueDto marqueDto) {
        System.out.println( marqueDto);
        marqueDto.setId(id);
        return service.saveOrUpdate(marqueDto);
    }
}
