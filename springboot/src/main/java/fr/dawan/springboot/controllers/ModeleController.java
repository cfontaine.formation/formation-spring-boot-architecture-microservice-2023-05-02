package fr.dawan.springboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.dawan.springboot.dtos.ModeleDto;
import fr.dawan.springboot.services.ModeleService;

@RestController
@RequestMapping("/api/v1/modeles")
public class ModeleController {

    @Autowired
    private ModeleService service;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ModeleDto> getAllModele(Pageable page) {
        return service.getAllModele(page);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ModeleDto> getModeleById(@PathVariable long id) {
        try {
            return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModeleDto addModele(@RequestBody ModeleDto modeleDto) {
        return service.saveOrUpdate(modeleDto);
    }

    @PostMapping(value = "/photo/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ModeleDto> addModeleImage(@PathVariable long id, @RequestParam("photo") MultipartFile file) {
        try {
            ModeleDto md = service.getById(id);
            md.setPhoto(file.getBytes());
            return ResponseEntity.ok(service.saveOrUpdate(md));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModeleDto udpateModele(@PathVariable long id, @RequestBody ModeleDto modeleDto) {
        modeleDto.setId(id);
        return service.saveOrUpdate(modeleDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteModele(@PathVariable long id) {
        if (service.deleteModele(id)) {
            return new ResponseEntity<String>("le modele est supprimé", HttpStatus.OK);
        }
        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
    }
}
