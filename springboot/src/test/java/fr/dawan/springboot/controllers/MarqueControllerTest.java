package fr.dawan.springboot.controllers;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.services.MarqueService;

@WebMvcTest(MarqueController.class)
@ActiveProfiles("TEST")
@AutoConfigureMockMvc(addFilters = false)
public class MarqueControllerTest {

    @MockBean
    private MarqueService service;

    @Autowired
    private MockMvc mockMvc;
    
    @Test
    public void getAllMarqueTest() throws Exception {
        List<MarqueDto> lstDto=new ArrayList<>();
        lstDto.add(new MarqueDto(1L,"marqueA") );
        lstDto.add(new MarqueDto(2L,"marqueB") );
        when(service.getAllMarque()).thenReturn(lstDto);
        String rep = mockMvc.perform(get("/api/v1/marques")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse().getContentAsString();
        try {
            JSONAssert.assertEquals("[{\"id\":1,\"nom\":\"marqueA\"},{\"id\":2,\"nom\":\"marqueB\"}]", rep, false);
        } catch (JSONException e) {
             fail();
        }
    }
    
    @Test
    public void getMarqueByIdTest() throws Exception {
        MarqueDto mDto = new MarqueDto(42L,"marqueA");
        when(service.getMarqueById(42L)).thenReturn(mDto);
        String rep = mockMvc.perform(get("/api/v1/marques/42")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse().getContentAsString();
         try {
            JSONAssert.assertEquals("{id:42,nom:\"marqueA\"}",
             rep, JSONCompareMode.STRICT);
        } catch (JSONException e) {
            fail();
        }
    }
}