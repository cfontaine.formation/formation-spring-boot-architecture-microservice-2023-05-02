package fr.dawan.springboot.services;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.entities.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.impl.MarqueServiceImpl;

@ExtendWith(MockitoExtension.class)
public class MarqueServiceTest {

    @Mock
    private MarqueRepository repo;

    @Mock
    private ModelMapper mapper;

    @InjectMocks
    private MarqueServiceImpl service;

    private Marque marque;

    private MarqueDto marqueDto;

    @BeforeEach
    void setUp() {
        marque = new Marque();
        marque.setId(1L);
        marque.setNom("Mazda");
        marqueDto = new MarqueDto(1L, "Mazda");
    }

    @Test
    void getAllMarqueTest() {
        List<Marque> lst = new ArrayList<>();
        lst.add(marque);
        when(repo.findAll()).thenReturn(lst);
        when(mapper.map(marque, MarqueDto.class)).thenReturn(marqueDto);

        List<MarqueDto> lstDto = service.getAllMarque();
        assertEquals(1, lstDto.size());
        assertThat(marqueDto, samePropertyValuesAs(lstDto.get(0)));
    }

    @Test
    void getMarqueByIdTest() {
         when(repo.findById(1L)).thenReturn(Optional.of(marque));
         when(mapper.map(marque, MarqueDto.class)).thenReturn(marqueDto);
        assertThat(marqueDto,samePropertyValuesAs(service.getMarqueById(1L)));
    }

    @Test
    void getMarqueByNameTest() {
        List<Marque> lst = new ArrayList<>();
        lst.add(marque);
        when(repo.findByNomLike("Maz")).thenReturn(lst);
        when(mapper.map(marque, MarqueDto.class)).thenReturn(marqueDto);
        List<MarqueDto> lstDto = service.getMarqueByName("Maz");
        assertEquals(1, lstDto.size());
        assertThat(marqueDto, samePropertyValuesAs(lstDto.get(0)));
    }

    @Test
    void deleteMarqueTest() {
        when(repo.existsById(1L)).thenReturn(true);
        when(repo.existsById(100L)).thenReturn(false);
        assertTrue(service.deleteMarque(1L));
        assertFalse(service.deleteMarque(100L));
    }

    @Test
    void saveOrUpdate() {
        when(repo.saveAndFlush(marque)).thenReturn(marque);
        when(mapper.map(marque, MarqueDto.class)).thenReturn(marqueDto);
        when(mapper.map(marqueDto, Marque.class)).thenReturn(marque);
        assertThat(marqueDto, samePropertyValuesAs(service.saveOrUpdate(marqueDto)));
    }
}
