package fr.dawan.springboot.repositories;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.springboot.entities.Marque;

@DataJpaTest
@ActiveProfiles("TEST")
public class MarqueRepositoryTest {
        @Autowired
        MarqueRepository repository;
        
        @Test
        public void repositoryTest() {
            assertNotNull(repository);
        }
        
        @Test
        public void findByNomLikeTest(){
            Marque m=new Marque();
            m.setId(2L);
            m.setNom("Fiat");
            
            List<Marque>lst=repository.findByNomLike("%iat%");
            
            assertEquals(1, lst.size());
            assertThat(m,samePropertyValuesAs(lst.get(0)));
        }

}
