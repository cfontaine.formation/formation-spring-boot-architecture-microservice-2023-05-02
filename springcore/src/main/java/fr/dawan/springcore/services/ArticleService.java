package fr.dawan.springcore.services;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.dawan.springcore.repositories.ArticleRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Service("service2") // => @Service, @Repository, @Controller , @Composant
                           // => un bean est créé             
public class ArticleService implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Autowired  // injection automatique de la dépendence
                // recherche dans le conteneur d'ioc d'un bean de type ArticleRepository => injection dans repository 
                // s'il y a plusieurs bean erreur, à moins de lever l'ambiguité avec @Primary
    
    //  @Autowired(required = false) // required = false-> dépendence optionnelle: s'il n'y a pas debean de type ArticleRepository
                                     // dans le conteneur -> repository= null et pas d'exception
    private ArticleRepository repository;

}
