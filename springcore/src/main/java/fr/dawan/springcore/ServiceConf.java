package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

import fr.dawan.springcore.services.ArticleService;

@Configuration                        // @Configuration => classe de configuration
@ComponentScan("fr.dawan.springcore") // => scan des classes du package pour trouver les composants
                                      // @Component, @Repository, @Controller,@Service
@Import(RepositoryConf.class) // => Charger des définitions de bean depuis une autre classe de configuration
public class ServiceConf {

    // Le paramètre repository2 de la méthode permet d'indiquer que le bean dépend
    // du Bean qui a pour nom repository2
//    @Bean
//    @Lazy // =>permet de retarder le chargement des singletons à leur première utilisation
//    public ArticleService service1(ArticleRepository repository2) {
//        return new ArticleService(repository2);
//    }
    
    @Bean
    @Lazy // =>permet de retarder le chargement des singletons à leur première utilisation
    public ArticleService service1() {
        return new ArticleService();
    }
}
