package fr.dawan.springcore.beans;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

// Exemple Lombok

@NoArgsConstructor  // => Création d'un constructeur sans paramètre 
@AllArgsConstructor // => Création d'un constructeur avec un paramètre pour chaque variable d'instance
@Getter             // => si on place @Getter sur la classe, on a un getter créer pour chaque variable d'instance 
@Setter             // => si on place @Setter sur la classe, on a un setter créer pour chaque variable d'instance 
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
// @EqualsAndHashCode => par défaut equals et hashCode, pour toutes les variables d'instances 
//                       On peut ignorer une variable d'instance avec @Exclude  
// avec le paramètre onlyExplicitlyIncluded = true => par défaut aucune variable d'instance n'est pas sélectionner
//                                                    Il faut ajouter les variables avec @Include
@ToString(onlyExplicitlyIncluded = true)
// @ToString => par défaut toutes les variables d'instances sont utilisées pour toString 
//           => On peut ignorer une variable d'instance avec @lombok.ToString.Exclude
// avec le paramètre onlyExplicitlyIncluded = true =>par défaut aucune variable n'est utilisé pour toString 
//                                                   Il faut ajouter les variables avec @Include
// @Data() => équivalant à @ToString, @Getter/@Setter @EqualsAndHashCode et @RequiredArgsConstructor
public class Article {
   // @Getter 
   // @Setter
    @Include
    @lombok.ToString.Include
    private double prix;
    
   // @Getter
    @Setter(value = AccessLevel.NONE)
   // @Exclude
   // @lombok.ToString.Exclude
   private String description;

    public Article(double prix) {
        this.prix = prix;
    }

}
