package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.Article;
import fr.dawan.springcore.repositories.ArticleRepository;
import fr.dawan.springcore.services.ArticleService;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        // Test Lombok
        Article a = new Article();
        a.setPrix(30.0);
        System.out.println(a.getPrix());
        System.out.println(a);

        Article a2 = new Article(30.0, "tv");
        System.out.println(a2);

        System.out.println(a.equals(a2));

        // Spring Core
        // Création du conteneur d'inversion de controle
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ServiceConf.class);

        // getBean -> permet de récupérer les instances des beans depuis le conteneur
        ArticleRepository r1 = ctx.getBean("repository1", ArticleRepository.class);
        System.out.println("repository 1 -> " + r1);

        ArticleRepository r2 = ctx.getBean("repository2", ArticleRepository.class);
        System.out.println("repository 2 -> " + r2);

        ArticleService s1 = ctx.getBean("service1", ArticleService.class);
        System.out.println("service 1 -> " + s1);

        ArticleService s2 = ctx.getBean("service2", ArticleService.class);
        System.out.println("service 2 -> " + s2);

        ((AbstractApplicationContext) ctx).close();
    }
}
