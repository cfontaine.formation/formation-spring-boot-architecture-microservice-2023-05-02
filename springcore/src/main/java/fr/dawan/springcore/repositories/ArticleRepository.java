package fr.dawan.springcore.repositories;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ArticleRepository implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private String url;
}
