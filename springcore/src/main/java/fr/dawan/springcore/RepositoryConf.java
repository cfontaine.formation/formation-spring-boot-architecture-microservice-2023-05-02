package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import fr.dawan.springcore.repositories.ArticleRepository;

@Configuration // @Configuration => classe de configuration
public class RepositoryConf {

    // Déclarer un bean une méthode annotée avec @Bean
    // Le type de retour est le type du bean, le nom du bean est le nom de la méthode
    @Bean
    public ArticleRepository repository1() { // un bean de type ArticleRepository et qui a pour nom repository1
        return new ArticleRepository("url1");
    }

    // On peut nommer le bean avec l'attribut name de @Bean
    @Bean(name = "repository2")
    @Primary
    public ArticleRepository repositoryDeux() { // un bean de type ArticleRepository et qui a pour nom repository2
        return new ArticleRepository("url2");
    }

}
